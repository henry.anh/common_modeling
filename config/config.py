import os

from common.common_keys import *


class Config:

    def __init__(self,
                 base_model: str = None,
                 checkpoint_dir: str = None,
                 output_dir: str = None,
                 ):

        self.base_model = base_model if base_model is not None else os.getenv(BASE_MODEL, "vilm/vinallama-2.7b")
        self.checkpoint_dir = checkpoint_dir if checkpoint_dir is not None else os.getenv(CHECKPOINT_DIR, "checkpoints")
        self.output_dir = output_dir if output_dir is not None else os.getenv(OUTPUT_DIR, "output_dir")

        self.lora_alpha = 128
        self.lora_dropout = 0.05
        self.rank = 64
        self.num_train_epochs = 1
        self.per_device_train_batch_size = 2
        self.gradient_accumulation_steps = 1
        self.optim = "adamw_bnb_8bit"
        self.save_steps = 100
        self.logging_steps = 200
        self.learning_rate = 1e-4
        self.warmup_ratio = 0.03
        self.lr_scheduler_type = "linear"

        self.target_length = 512

        self.ultrachat_dataset = "dataset/ultrachat/"
        self.billocr_dataset = "dataset/billocr/"
        self.paraphrase_dataset = "dataset/paraphrase/"
        self.summary_dataset = "dataset/summary/"
        self.translation_dataset = "dataset/translation/"
