import os

import torch
from torch import nn
from transformers.integrations import MLflowCallback
from transformers import (
    AutoTokenizer, AutoModelForCausalLM,
    BitsAndBytesConfig, DataCollatorForLanguageModeling,
    TrainingArguments
)
from transformers.trainer_pt_utils import get_parameter_names
from trl import setup_chat_format, SFTTrainer
from peft import LoraConfig, get_peft_model
from accelerate import dispatch_model
import bitsandbytes as bnb

from common.common_keys import *
from config.config import Config
from llm.instruct_dataset import InstructDataset


def print_trainable_parameters(model):
    trainable_params = 0
    all_params = 0
    for _, param in model.named_parameters():
        all_params += param.numel()
        if param.requires_grad:
            trainable_params += param.numel()

    print(f"Trainable params: {trainable_params} || All params: {all_params} || Trainable%: {trainable_params/all_params*100}%")


def train(config: Config):
    tokenizer = AutoTokenizer.from_pretrained(config.base_model, cache_dir=config.checkpoint_dir)
    device_map = {"": int(os.environ[LOCAL_RANK])}
    num_new_tokens = 0
    if tokenizer.pad_token is None:
        num_new_tokens += tokenizer.add_special_tokens({PAD_TOKEN: f'[{PAD}]'})
    max_seq_length = 1024

    quantization_config = BitsAndBytesConfig(
        load_in_4bit=True,
        load_in_8bit=False,
        llm_int8_threshold=6.0,
        bnb_4bit_compute_dtype=torch.float16,
        bnb_4bit_use_double_quant=True,
        bnb_4bit_quant_type="nf4" # ("fp4", "nf4")
    )
    print(f"LOCAL RANK: {os.environ[LOCAL_RANK]}")
    model = AutoModelForCausalLM.from_pretrained(
        config.base_model, cache_dir=config.checkpoint_dir,
        quantization_config=quantization_config,
        # attn_implementation="flash_attention_2",
        device_map=device_map
    )
    model, tokenizer = setup_chat_format(model=model, tokenizer=tokenizer)
    tokenizer.pad_token_id = 46303 # only use for ultrachat

    lora_config = LoraConfig(
        r=config.rank,
        lora_alpha=config.lora_alpha,
        lora_dropout=config.lora_dropout,
        modules_to_save=["embed_tokens"],
        bias="none",
        task_type="CAUSAL_LM"
    )

    model = get_peft_model(model, lora_config)
    model = dispatch_model(model=model, device_map=device_map)
    dataset = InstructDataset(config=_config, tokenizer=tokenizer, max_length=max_seq_length, target_length=config.target_length)
    model.train()
    print_trainable_parameters(model=model)
    torch.backends.cuda.matmul.allow_tf32 = True
    torch.backends.cuda.allow_tf32 = True

    training_arguemnts = TrainingArguments(
        output_dir=config.output_dir,
        per_device_train_batch_size=config.per_device_train_batch_size,
        gradient_accumulation_steps=config.gradient_accumulation_steps,
        gradient_checkpointing=True,
        num_train_epochs=config.num_train_epochs,
        gradient_checkpointing_kwargs={"use_reentrant": False},
        learning_rate=config.learning_rate,
        lr_scheduler_type=config.lr_scheduler_type,
        # optim=config.optim,
        save_steps=config.save_steps,
        logging_steps=config.logging_steps,
        warmup_ratio=config.warmup_ratio,
        bf16=False,
        save_total_limit=2, neftune_noise_alpha=5
    )

    decay_parameters = get_parameter_names(model, [nn.LayerNorm])
    decay_parameters = [name for name in decay_parameters if BIAS not in name]
    optimizer_grouped_parameters = [
        {
            PARAMS: [p for n, p in model.named_parameters() if n in decay_parameters],
            WEIGHT_DECAY: training_arguemnts.weight_decay
        },
        {
            PARAMS: [p for n, p in model.named_parameters() if n not in decay_parameters],
            WEIGHT_DECAY: 0.0
        }
    ]
    adam_bnb_optim = bnb.optim.AdamW8bit(
        optimizer_grouped_parameters,
        betas=(training_arguemnts.adam_beta1, training_arguemnts.adam_beta2),
        eps=training_arguemnts.adam_epsilon,
        lr=training_arguemnts.learning_rate
    )

    data_collator = DataCollatorForLanguageModeling(tokenizer=tokenizer, mlm=False, return_tensors="pt")
    trainer = SFTTrainer(
        model=model, tokenizer=tokenizer,
        train_dataset=dataset,
        optimizers=(adam_bnb_optim, None),
        peft_config=lora_config,
        data_collator=data_collator,
        dataset_text_field="text",
        args=training_arguemnts
    )
    trainer.remove_callback(MLflowCallback)
    trainer.train()


if __name__ == "__main__":
    _config = Config()
    train(config=_config)