import os
import logging
from logging.handlers import RotatingFileHandler


def init_logging(log_name: str = "train"):
    log_file = f"logs/{log_name}.log"
    parent_path, _ = os.path.split(log_file)
    os.makedirs(parent_path, exist_ok=True)
    logger = logging.getLogger(log_name)
    logger.setLevel(level=logging.DEBUG)

    formatter = logging.Formatter("%(asctime)s:%(name)s:%(message)s")

    file_handler = RotatingFileHandler(log_file, maxBytes=1024*1024*4, backupCount=10)
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)

    logger.addHandler(file_handler)
    return logger
