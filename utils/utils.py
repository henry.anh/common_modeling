import random
import re

from common.common_regex import VOWELS_PATTERN, CHAR1252, CHARUTF8


def norm_text(line):
    def convert_unicode(text: str) -> str:
        """
        :param text: input text
        :return: utf-8 encoded text
        """
        character_map = dict()


        for i in range(len(CHAR1252)):
            character_map[CHAR1252[i]] = CHARUTF8[i]

        return VOWELS_PATTERN.sub(lambda x: character_map[x.group()], text)
    line = line.strip()
    line = convert_unicode(line)
    line = '\n'.join([i for i in line.split('\n')])
    line = re.sub("\n +", "\n", line)
    line = re.sub(" +\n", "\n", line)
    line = re.sub("\n{3,}", '\n\n', line)
    line = re.sub(" +", " ", line)
    return line


def get_random_index(idx: int):
    return random.randint(0, idx)