BASE_MODEL = "BASE_MODEL"
CHECKPOINT_DIR = "CHECKPOINT_DIR"
OUTPUT_DIR = "OUTPUT_DIR"

INSTRUCTION = "instruction"
ROLE = "role"
USER = "user"
CONTENT = "content"
INPUT = "input"
OUTPUT = "output"
EXAMPLES = "examples"
SYSTEM = "system"
ASSISTANT = "assistant"
INPUT_IDS = "input_ids"
ATTENTION_MASK = "attention_mask"
PAD_TOKEN = "pad_token"
PAD = "PAD"
LOCAL_RANK = "LOCAL_RANK"
BIAS = "bias"
PARAMS = "params"
WEIGHT_DECAY = "weight_decay"
BETAS = "betas"
EPS = "eps"
LR = "lr"

FORMAT_INSTRUCTION_JSON = "format_instruction_json"
CONVERSATION_INSTRUCTION = "conversation_instruction"
CONVERSATION = "conversation"
CONVERSATION_EN = "conversation_en"
PROPERTIES = "properties"
TYPE = "type"
DESCRIPTION = "description"
JSON = "json"
CSV = "csv"