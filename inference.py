import torch
from transformers import AutoTokenizer, AutoModelForCausalLM, BitsAndBytesConfig
from peft import LoraConfig, PeftModel
from trl import setup_chat_format

from config.config import Config

config = Config()
quantization_config = BitsAndBytesConfig(
        load_in_4bit=True,
        load_in_8bit=False,
        llm_int8_threshold=6.0,
        bnb_4bit_compute_dtype=torch.float16,
        bnb_4bit_use_double_quant=True,
        bnb_4bit_quant_type="nf4" # ("fp4", "nf4")
    )

lora_config = LoraConfig(
        r=config.rank,
        lora_alpha=config.lora_alpha,
        lora_dropout=config.lora_dropout,
        bias="none",
        task_type="CAUSAL_LM"
    )

tokenizer = AutoTokenizer.from_pretrained(config.base_model,cache_dir=config.checkpoint_dir)
model = AutoModelForCausalLM.from_pretrained(config.base_model, cache_dir=config.checkpoint_dir, quantization_config=quantization_config)
model, tokenizer = setup_chat_format(model=model, tokenizer=tokenizer)
tokenizer.pad_token_id = 46303  # only use for ultrachat
model = PeftModel.from_pretrained(model, config.output_dir+ "/checkpoint-101200").to("cuda:0")

context = "Chelsea sẽ tăng lương gấp ba để trói chân Courtois. Tuyển thủ Bỉ sắp ký gia hạn thêm năm năm hợp đồng, sau khi được chính HLV Mourinho xác nhận là thủ môn số một của Chelsea. Hợp đồng hiện tại giữa Thibaut Courtois với Chelsea vẫn còn hiệu lực đến hè 2016, nhưng mức lương của thủ môn người Bỉ chỉ là 50.000 đôla mỗi tuần. Chelsea, từ đầu hè này, đã mời gọi Courtois gia hạn hợp đồng sau khi xác định sẽ đòi lại thủ môn 22 tuổi vừa vô địch La Liga, vào chung kết Champions League trong màu áo Atletico Madrid. Nhưng Courtois kiên quyết từ chối vì muốn làm rõ vị thế khi trở lại đội bóng chủ quản và nhấn mạnh lập trường rằng anh không trở lại chỉ đế làm dự bị cho đàn anh Petr Cech. Vị thế ấy của Courtois được làm rõ hôm thứ hai vừa qua khi anh được Mourinho chọn bắt chính trong trận ra quân Ngoại hạng Anh - thắng 3-1 trên sân Burnley."
question = ""
prompt = f"<|im_start|>system\nDựa trên context của người dùng, trích xuất thông tin và tổng hợp thông tin dưới đây:<|im_end|>\n" \
         f"<|im_start|>user\n: {context}<|im_end|>\n" \
         f"<|im_start|>{question}\nassistant\n:"

max_new_tokens=128
do_sample=False
repetition_penalty=1.2
num_beams=5
eos_token_id=2

encoded_ids = tokenizer.encode(prompt, add_special_tokens=False, return_tensors="pt").to("cuda:0")
output_ids = model.generate(
    encoded_ids,
    max_new_tokens=max_new_tokens,
    min_new_tokens=1,
    do_sample=do_sample,
    repetition_penalty=repetition_penalty,
    num_beams=num_beams,
    eos_token_id=tokenizer.eos_token_id,
    remove_invalid_values=True,
    renormalize_logits=True,
    use_cache=True
)
results = tokenizer.decode(output_ids[0])
print(results)

