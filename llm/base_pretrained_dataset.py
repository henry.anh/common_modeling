import glob
import time
from enum import auto
from fastapi_utils.enums import StrEnum

from llm.utils import *


class TaskType(StrEnum):
    conver = auto()
    output_format = auto()
    standard = auto()


class BasePretrainedDataset:

    def __init__(self, data_path: str, max_length: int, tokenizer=None, amount=None):

        self.all_mappings = []
        self.data_path = data_path
        self.max_length = max_length
        self.tokenizer = tokenizer

        self.base_instructions = json.load(open(f"llm/json_file/base_instructions.json", "r", encoding="utf8"))

        for p in glob.glob(f"{data_path}/*_mapping.json"):
            data_file = json.load(open(p, "r", encoding="utf8"))
            self.all_mappings.extend(data_file[:-int(len(data_file)*0.05)])

        if amount is not None:
            self.all_mappings = random.sample(self.all_mappings, min(amount, len(self.all_mappings)))

    def __len__(self) -> int:
        return len(self.all_mappings)

    def get_sample_from_id(self, idx):
        sample = self.all_mappings[idx]
        path = sample['path']
        path = path.split('/')[-1]
        while True:
            try:
                with open(f"{self.data_path}/{path}", encoding='utf8') as f:
                    f.seek(sample['offset'])
                    sample = f.read(sample['size'])
                    sample = json.loads(sample)
            except Exception as e:
                print(e)
                print(sample)
                time.sleep(10)
            else:
                break
        return sample, path
