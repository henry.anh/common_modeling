import random
from typing import Dict
from pydantic import create_model
from pydantic.fields import FieldInfo

from llm.base_pretrained_dataset import (
    BasePretrainedDataset, TaskType,
    get_gibberish_key, get_format_description, format_output
)
from llm.llm_dataset.billocr.billocr_instruction import *
from llm.llm_dataset.billocr.billocr_common_keys import *


class BillOCRDataset(BasePretrainedDataset):

    @staticmethod
    def get_avail_tasks():
        return [TaskType.output_format]

    def get_random_attribute(self, origin_prefix, value_prefix):
        language = random.choice([VIETNAMESE, ENGLISH])
        origin_prefix = random.choice(origin_prefix[language])
        value_prefix = random.choice(value_prefix[language])
        if language == ENGLISH:
            pref = origin_prefix + ' ' + value_prefix
        else:
            pref = origin_prefix + ' ' + value_prefix
        if random.random() < 0.4:
            pref = pref.replace(' ', '_')
        if random.random() < 0.1:
            pref = get_gibberish_key()
        return pref

    def get_sample(self, idx, task_type) -> Dict:
        sample, sample_path = self.get_sample_from_id(idx)
        output_type = "json"
        if random.random() < 5:  # CURRENTLY ONLY HANDLING LIST IN DICT, THE CASE IN ELSE IS DICT IN DICT
            sender_name = self.get_random_attribute(synonyms[SENDER], synonyms[NAME])
            sender_address = self.get_random_attribute(synonyms[SENDER], synonyms[ADDRESS])
            sender_phone = self.get_random_attribute(synonyms[SENDER], synonyms[PHONE])
            receiver_name = self.get_random_attribute(synonyms[RECEIVER], synonyms[NAME])
            receiver_address = self.get_random_attribute(synonyms[RECEIVER], synonyms[ADDRESS])
            receiver_phone = self.get_random_attribute(synonyms[RECEIVER], synonyms[PHONE])

            data_model = {
                sender_name: (random.choice([str, list]), FieldInfo(default='', description=random.choice(
                    synonyms[NAME][EXPLANATION]).replace(DEST,
                                                             random.choice(synonyms[SENDER][VIETNAMESE])))),
                sender_address: (random.choice([str, list]), FieldInfo(default='', description=random.choice(
                    synonyms[ADDRESS][EXPLANATION]).replace(DEST,
                                                                random.choice(synonyms[SENDER][VIETNAMESE])))),
                sender_phone: (random.choice([str, list]), FieldInfo(default='', description=random.choice(
                    synonyms[PHONE][EXPLANATION]).replace(DEST,
                                                              random.choice(synonyms[SENDER][VIETNAMESE])))),
                receiver_name: (random.choice([str, list]), FieldInfo(default='', description=random.choice(
                    synonyms[NAME][EXPLANATION]).replace(DEST,
                                                             random.choice(synonyms[RECEIVER][VIETNAMESE])))),
                receiver_address: (random.choice([str, list]), FieldInfo(default='', description=random.choice(
                    synonyms[ADDRESS][EXPLANATION]).replace(DEST,
                                                                random.choice(synonyms[RECEIVER][VIETNAMESE])))),
                receiver_phone: (random.choice([str, list]), FieldInfo(default='', description=random.choice(
                    synonyms[PHONE][EXPLANATION]).replace(DEST,
                                                              random.choice(synonyms[RECEIVER][VIETNAMESE])))),
            }
            data_model_keys = list(data_model.keys())
            random.shuffle(data_model_keys)
            response = {
                sender_name: sample[OUTPUT][SENDER][NAME] if data_model[sender_name][0] is str else [
                    sample[OUTPUT][SENDER][NAME]],
                sender_address: sample[OUTPUT][SENDER][ADDRESS] if data_model[sender_address][0] is str else [
                    sample[OUTPUT][SENDER][ADDRESS]],
                sender_phone: sample[OUTPUT][SENDER][PHONE_NUMBER] if data_model[sender_phone][0] is str else [
                    sample[OUTPUT][SENDER][PHONE_NUMBER]],
                receiver_name: sample[OUTPUT][RECEIVER][NAME] if data_model[receiver_name][0] is str else [
                    sample[OUTPUT][RECEIVER][NAME]],
                receiver_address: sample[OUTPUT][RECEIVER][ADDRESS] if data_model[receiver_address][
                                                                                 0] is str else [
                    sample[OUTPUT][RECEIVER][ADDRESS]],
                receiver_phone: sample[OUTPUT][RECEIVER][PHONE_NUMBER] if data_model[receiver_phone][
                                                                                    0] is str else [
                    sample[OUTPUT][RECEIVER][PHONE_NUMBER]],
            }
            used_keys = []
            new_response, new_data_model = {}, {}
            for k in data_model_keys:
                if random.random() < 0.1 and len(data_model.keys()) > 1:
                    data_model.pop(k)
                    continue
                new_response[k] = response[k]
                new_data_model[k] = data_model[k]
                used_keys.append(k)
            response, data_model = new_response, new_data_model
            data_model = create_model("CCModel", **data_model)
        format_instruct = get_format_description(base_instructions=self.base_instructions, data_model=data_model, kind=output_type)
        response = format_output(response, output_type)
        bill_text = '\n'.join(sample[INPUT])
        instruction = random.choice(system)
        input_text = random.choice(instructions) + "\n\n" + bill_text + '\n\n' + format_instruct
        messages = [{
            ROLE: USER,
            CONTENT: input_text
        }]
        full_sample = {
            INSTRUCTION: instruction,
            INPUT: messages,
            OUTPUT: response,
            EXAMPLES: []
        }
        return full_sample
