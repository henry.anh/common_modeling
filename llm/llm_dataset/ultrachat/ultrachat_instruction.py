response_synonym = ["trả lời", "đáp án", "kết quả", "answer", "result", "response", "trả lời", "A", 'answer', 'tl', "A",
                    "a", "ans", "res"]

passage_synnonyms = ["đoạn văn", "văn bản", "chủ đề", "thông tin", "đoạn văn bản", "chủ đề trò chuyện"]
conversation_synoyms = ["hội thoại", "Hội thoại", "Trò chuyện", "trò chuyện", "cuộc nói chuyện", "đoạn hội thoại"]
speaker_names = [["A", "B"], ["User", "AI"], ["Human", "Assistant"]]

all_instructions = [
    "Dựa vào ngữ cảnh, vui lòng tiếp tục đoạn trò chuyện một cách thích hợp.",
    "Xem xét ngữ cảnh và lịch sử trò chuyện, tiếp tục cuộc hội thoại một cách thích hợp.",
    "Hãy tiếp tục cuộc hội thoại một cách phù hợp dựa trên bối cảnh",
    "Dựa vào tình hình và lịch sử trò chuyện, tiếp tục cuộc hội thoại một cách hợp lý.",
    "Vui lòng tiếp tục trò chuyện một cách phù hợp với ngữ cảnh và lịch sử.",
    "Tùy thuộc vào tình huống và cuộc trò chuyện, hãy tiếp tục cuộc trò chuyện này.",
    "Hãy tiếp tục cuộc trò chuyện theo cách thích hợp dựa trên ngữ cảnh",
    "Dựa vào bối cảnh và quá trình trò chuyện, hãy tiếp tục cuộc hội thoại một cách thích hợp.",
    "Hãy tiếp tục đoạn trò chuyện theo cách hợp lý dựa vào ngữ cảnh và lịch sử.",
    "Tùy thuộc vào tình huống, vui lòng tiếp tục cuộc hội thoại.",
    "Dựa vào tình hình và lịch sử trò chuyện, hãy tiếp tục cuộc trò chuyện một cách thích hợp.",
    "Xem xét ngữ cảnh và cuộc trò chuyện trước đó, tiếp tục đoạn hội thoại.",
    "Hãy tiếp tục cuộc hội thoại một cách phù hợp dựa trên bối cảnh",
    "Dựa vào bối cảnh và quá trình trò chuyện, tiếp tục cuộc hội thoại theo cách thích hợp.",
    "Vui lòng tiếp tục trò chuyện một cách thích hợp với ngữ cảnh và lịch sử.",
    "Tùy thuộc vào tình huống, hãy tiếp tục cuộc hội thoại một cách hợp lý.",
    "Hãy tiếp tục cuộc trò chuyện theo cách thích hợp dựa vào ngữ cảnh và lịch sử.",
    "Dựa vào bối cảnh và quá trình trò chuyện, hãy tiếp tục đoạn hội thoại một cách thích hợp.",
    "Xem xét ngữ cảnh, tiếp tục cuộc trò chuyện một cách phù hợp.",
    "Hãy tiếp tục cuộc hội thoại một cách thích hợp dựa trên tình huống"
]
