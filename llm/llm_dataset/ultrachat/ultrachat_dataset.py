import random
from typing import Dict

from common.common_keys import *
from llm.base_pretrained_dataset import (
    BasePretrainedDataset, TaskType,
    get_conversation_instruction
)


class UltraChatDataset(BasePretrainedDataset):

    @staticmethod
    def get_avail_tasks():
        return [TaskType.conver]

    def turns_to_examples(self, context_lst, context_lst_en):
        examples = []
        input_length = len(self.tokenizer.encode('\n'.join(context_lst)))
        while input_length > self.max_length * 0.8 and len(context_lst) > 2:
            context_lst = random.choice([context_lst[2:], context_lst[:-2]])
            input_length = len(self.tokenizer.encode('\n'.join(context_lst)))
        response = context_lst[-1]
        context_lst = context_lst[:-1]
        conv_turn = []
        conv_turn_en = []
        for turn_id, (turn, turn_en) in enumerate(zip(context_lst[:-1], context_lst_en[:-1])):
            conv_turn.append(turn.strip())
            conv_turn_en.append(turn_en.strip())
            if turn_id % 2 != 0:
                turn_sample = [
                    {
                        ROLE: USER,
                        CONTENT: conv_turn[0] if random.random() < 0.7 else conv_turn_en[0]
                    },
                    {
                        ROLE: ASSISTANT,
                        CONTENT: conv_turn[1]
                    }
                ]
                examples.extend(turn_sample)
                conv_turn = []

        turns = examples + [{ROLE: USER, CONTENT: context_lst[-1]}]
        return turns, response

    def get_sample(self, idx, task_type) -> Dict:
        sample, sample_path = self.get_sample_from_id(idx)

        messages, response = self.turns_to_examples(sample[CONVERSATION], sample[CONVERSATION_EN])
        instruction = get_conversation_instruction(base_instructions=self.base_instructions)

        full_sample = {
            INSTRUCTION: instruction,
            INPUT: messages,
            OUTPUT: response,
            EXAMPLES: []
        }
        return full_sample