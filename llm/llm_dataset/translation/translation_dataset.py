from typing import Dict, Any

from llm.base_pretrained_dataset import BasePretrainedDataset, TaskType
from llm.llm_dataset.translation.translation_common_keys import *
from llm.utils import *


class TranslationDataset(BasePretrainedDataset):

    @staticmethod
    def get_avail_tasks():
        return [TaskType.conver]

    def get_sample(self, idx, task_type) -> Dict[str, Any]:
        sample, sample_path = self.get_sample_from_id(idx=idx)
        messages = sample[MESSAGES]
        if messages[-1][ROLE] == ASSISTANT:
            response = messages[-1][CONTENT]
            messages = messages[:-1]
        if messages[0][ROLE] == SYSTEM and messages[0][CONTENT]:
            system_en = sample.get(SYSTEM_EN)
            if system_en is not None and random.random() < 0.2:
                instruction = system_en
            else:
                instruction = messages[0][CONTENT]
            messages = messages[1:]
        else:
            instruction = get_conversation_instruction(base_instructions=self.base_instructions)

        full_sample = {
            INSTRUCTION: instruction,
            INPUT: messages,
            OUTPUT: response,
            EXAMPLES: []
        }
        return full_sample
