paraphrase_synonym = ["trả lời", "answer", "result", "response", "res", "paraphrase", "viết lại", "rephrase"]


paraphrase_desc = [
    "viết lại câu đã cho",
    "câu được viết lại"
]

paraphrase_instructions = [
    "Vui lòng thay đổi cách sử dụng từ để viết lại câu sau [times], đồng thời giữ nguyên ý nghĩa của câu gốc.",
    "Hãy sáng tạo bằng cách thay đổi từ ngữ khi viết lại câu sau [times], nhưng vẫn giữ nguyên ý nghĩa ban đầu.",
    "Viết lại câu dưới đây [times] bằng cách sử dụng từ ngữ khác, nhưng vẫn giữ nguyên ý nghĩa của câu gốc.",
    "Xin vui lòng thực hiện việc thay đổi cách sử dụng từ ngữ để viết lại câu sau [times], nhưng đảm bảo vẫn giữ được ý nghĩa ban đầu.",
    "Đề nghị bạn sử dụng từ ngữ mới để viết lại câu sau [times], đồng thời bảo toàn ý nghĩa ban đầu của câu.",
    "Yêu cầu bạn thay đổi cách sử dụng từ ngữ khi viết lại câu sau [times], nhưng phải giữ nguyên nội dung ý nghĩa của câu gốc.",
    "Hãy dùng từ ngữ khác để viết lại câu sau [times], nhưng đảm bảo ý nghĩa của câu ban đầu không thay đổi.",
    "Xin vui lòng viết lại câu dưới đây bằng cách sử dụng từ vựng khác [times], nhưng đảm bảo ý nghĩa của câu gốc vẫn được giữ nguyên.",
    "Mong bạn thay đổi cách sử dụng từ ngữ để viết lại câu sau [times], nhưng vẫn giữ nguyên ý nghĩa của câu ban đầu.",
    "Vui lòng thực hiện việc sử dụng từ ngữ khác nhau khi viết lại câu sau [times], nhưng đảm bảo ý nghĩa của câu gốc không bị thay đổi.",
    "Hãy sáng tạo bằng cách sử dụng từ ngữ mới để viết lại câu sau [times], nhưng vẫn giữ nguyên ý nghĩa của câu gốc.",
    "Yêu cầu bạn thay đổi cách sử dụng từ ngữ khi viết lại câu sau [times], nhưng đảm bảo ý nghĩa của câu ban đầu không thay đổi.",
    "Vui lòng viết lại câu dưới đây [times] bằng cách sử dụng từ ngữ khác, nhưng đảm bảo ý nghĩa của câu gốc vẫn được giữ nguyên.",
    "Hãy dùng từ ngữ khác để viết lại câu sau [times], nhưng đảm bảo ý nghĩa của câu ban đầu không bị thay đổi.",
    "Xin vui lòng thay đổi cách sử dụng từ để viết lại câu sau [times], đồng thời giữ nguyên ý nghĩa của câu gốc.",
    "Mong bạn sáng tạo bằng cách thay đổi từ ngữ khi viết lại câu sau [times], nhưng vẫn giữ nguyên ý nghĩa ban đầu của câu.",
    "Viết lại câu dưới đây bằng cách sử dụng từ ngữ khác [times], nhưng vẫn giữ nguyên ý nghĩa của câu gốc.",
    "Xin vui lòng thực hiện việc thay đổi cách sử dụng từ ngữ để viết lại câu sau [times], nhưng đảm bảo vẫn giữ được ý nghĩa ban đầu.",
    "Đề nghị bạn sử dụng từ ngữ mới để viết lại câu sau [times], đồng thời bảo toàn ý nghĩa ban đầu của câu.",
    "Yêu cầu bạn thay đổi cách sử dụng từ ngữ khi viết lại câu sau [times], nhưng phải giữ nguyên nội dung ý nghĩa của câu gốc.",
    "Hãy dùng từ ngữ khác để viết lại câu sau [times], nhưng đảm bảo ý nghĩa của câu ban đầu không thay đổi.",
    "Mong bạn thay đổi cách sử dụng từ ngữ để viết lại câu sau [times], nhưng vẫn giữ nguyên ý nghĩa của câu ban đầu."
]