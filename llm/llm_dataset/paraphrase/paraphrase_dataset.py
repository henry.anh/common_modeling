from pydantic.fields import FieldInfo
from pydantic import create_model

from llm.utils import *
from llm.base_pretrained_dataset import BasePretrainedDataset, TaskType
from llm.llm_dataset.paraphrase.paraphrase_instruction import *
from llm.llm_dataset.paraphrase.paraphrase_common_keys import *


class ParaphraseDataset(BasePretrainedDataset):

    @staticmethod
    def get_avail_tasks():
        return [TaskType.conver, TaskType.output_format]

    def get_sample(self, idx, task_type):
        sample, sample_path = self.get_sample_from_id(idx=idx)
        instruction = random.choice(paraphrase_instructions)
        answer_synonym = random.choice(paraphrase_synonym)
        times = ""
        if random.random() < 0.6:
            output_type = str
            response_paraphrase = random.choice(sample[PARAPHRASES])
        else:
            output_type = list
            response_paraphrase = random.sample(sample[PARAPHRASES], random.randint(1, len(sample[PARAPHRASES])))
            times = str(len(response_paraphrase)) + " lần"
        instruction = instruction.replace("[times]", times)
        if random.random() < 0.05:
            answer_synonym = get_gibberish_key()
        format_instruction = ""
        if task_type == TaskType.output_format:
            data_model = {
                answer_synonym: (
                    output_type,
                    FieldInfo(default="", description=random.choice(paraphrase_desc))
                )
            }
            data_model = create_model("CCModel", **data_model)
            format_instruction = get_format_description(
                base_instructions=self.base_instructions,
                data_model=data_model,
                kind=JSON
            )
            response = {
                answer_synonym: response_paraphrase
            }
            response = format_output(data=response, data_type=JSON)
        elif isinstance(response_paraphrase, list):
            response = [f"{idx+1}. {par}" for idx, par in enumerate(response_paraphrase)]
            response = "\n".join(response)
        else:
            response = response_paraphrase
        body = instruction + "\n\n" + sample[TEXT] + (f"\n\n" + format_instruction if format_instruction else "")
        input_text = [{
            ROLE: USER,
            CONTENT: body
        }]

        full_sample = {
            INSTRUCTION: get_conversation_instruction(base_instructions=self.base_instructions),
            INPUT: input_text,
            OUTPUT: response,
            EXAMPLES: []
        }
        return full_sample


