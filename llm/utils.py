import json
import random
import pandas as pd

from common.common_keys import *

def json_to_csv(json_data):
    new_json = {}
    for k, v in json_data.items():
        if isinstance(v, str):
            new_json.update({k: [v]})
        elif isinstance(v, list):
            new_json.update({k: [', '.join(v)]})
    df = pd.DataFrame(data=new_json)
    return df.to_markdown()


def get_random_capitalize(word):
    return random.choice([word.upper(), word.lower(), word.title(), word.capitalize()])


def get_gibberish_key(min_length=1, max_length=12):
    letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ '
    text = ""
    for idx in range(max_length - min_length):
        if idx != 0:
            text += random.choice(letters)
        else:
            text += random.choice(letters[:-1])
    return text


def get_format_description(base_instructions, data_model, kind:str = JSON):
    if data_model is None:
        return random.choice(base_instructions[FORMAT_INSTRUCTION_JSON])

    data_desc = data_model.schema()[PROPERTIES]
    data_desc = {k: f"{v[TYPE]}, {v[DESCRIPTION]}" if DESCRIPTION in v and v[DESCRIPTION] else v[TYPE] for
                 k, v in data_desc.items()}
    if kind == JSON:
        return random.choice(base_instructions[FORMAT_INSTRUCTION_JSON]) + ' ' + json.dumps(data_desc,
                                                                                                   ensure_ascii=False)
    elif kind == CSV:
        return random.choice(base_instructions[FORMAT_INSTRUCTION_JSON]) + '\n' + json_to_csv(data_desc)


def format_output(data, data_type):
    if data_type == JSON:
        return json.dumps(data, ensure_ascii=False)
    elif data_type == CSV:
        return "\n" + json_to_csv(data)


def get_conversation_instruction(base_instructions):
    conversation = random.choice(base_instructions[CONVERSATION_INSTRUCTION])
    return conversation