import random
import re
from typing import Dict
from torch.utils.data import Dataset

from common.common_keys import *
from utils.init_logging import init_logging
from utils.utils import norm_text, get_random_index
from llm.llm_dataset.ultrachat.ultrachat_dataset import UltraChatDataset
from llm.llm_dataset.billocr.billocr_dataset import BillOCRDataset
from llm.llm_dataset.summary.summary_dataset import SummaryDataset
from llm.llm_dataset.paraphrase.paraphrase_dataset import ParaphraseDataset
from llm.llm_dataset.translation.translation_dataset import TranslationDataset


class InstructDataset(Dataset):

    def __init__(self, config, tokenizer=None, max_length: int = None, target_length: int = None):
        super(InstructDataset, self).__init__()

        self.all_datasets = {
            "ultrachat": UltraChatDataset(
                data_path=config.ultrachat_dataset,
                tokenizer=tokenizer,
                max_length=max_length,
                amount=1000
            ),
            "billocr": BillOCRDataset(
                data_path=config.billocr_dataset,
                max_length=max_length,
                amount=1000
            ),
            "paraphrases": ParaphraseDataset(
                data_path=config.paraphrase_dataset,
                max_length=max_length,
                amount=1000
            ),
            "summary": SummaryDataset(
                data_path=config.summary_dataset,
                max_length=max_length,
                amount=1000
            ),
            "translation": TranslationDataset(
                data_path=config.translation_dataset,
                max_length=max_length,
                amount=1000
            )
        }
        self.tokenizer = tokenizer
        self.max_length = max_length
        self.target_length = target_length
        self.logger = init_logging()

    def __len__(self):
        return sum([len(i) for i in self.all_datasets.values()])

    def idx_to_class(self, idx):
        cur_idx = idx
        for dts_name, dts_value in self.all_datasets.items():
            if cur_idx < len(dts_value):
                task_type = random.choice(dts_value.get_avail_tasks())
                self.logger.info(dts_value)
                try:
                    return dts_value.get_sample(idx=cur_idx, task_type=task_type), task_type, dts_name
                except Exception as e:
                    print(e)
                    print(idx, ' ', cur_idx)
                    print(dts_name)
                    return None, None, None
            else:
                cur_idx = cur_idx - len(dts_value)
        return None, None, None

    def get_sample_from_index(self, idx):
        while True:
            if idx >= self.__len__():
                idx = get_random_index(idx=self.__len__())
            sample, task_type, dataset_name = self.idx_to_class(idx=idx)
            return sample, task_type, dataset_name

    def __getitem__(self, idx) -> Dict:
        sample = None
        while sample is None:
            sample, task_type, dataset_name = self.get_sample_from_index(idx=idx)
            if sample is None:
                idx = random.randint(0, len(self))

        instruction = norm_text(sample[INSTRUCTION])
        context = [
            {
                ROLE: message[ROLE],
                CONTENT: norm_text(message[CONTENT])
            }
            for message in sample[INPUT]
        ]
        output = norm_text(sample[OUTPUT])
        examples = sample[EXAMPLES]
        history = context
        if instruction:
            system = {
                ROLE: SYSTEM,
                CONTENT: instruction
            }
        cut_output = False
        tries = 0
        while True:
            tries += 1
            output_ids = self.tokenizer.encode(output, add_special_tokens=False)
            if len(output_ids) > self.target_length:
                output_ids = output_ids[:self.target_length]
                output = self.tokenizer.decode(output_ids)
            output_ids = self.tokenizer.encode(output, add_special_tokens=False)
            if history and history[0][ROLE] == ASSISTANT:
                history = history[1:]
            full_sample = self.tokenizer.apply_chat_template(
                [system] +
                history +
                [{
                    ROLE: ASSISTANT,
                    CONTENT: output
                }],
                tokenize=False
            )
            sample_ids = self.tokenizer.encode(full_sample, add_special_tokens=False)
            if len(sample_ids) <= self.max_length:
                break
            remaining = len(sample_ids) - self.max_length + 20
            if tries == 10:
                sample_ids = sample_ids[-self.max_length:]
                full_sample = self.tokenizer.decode(sample_ids)
                break
            while len(history) > 1:
                example = history.pop(0)
                remaining -= len(self.tokenizer.encode(example[CONTENT], add_special_tokens=False)) - 3
                if remaining <= 0:
                    break
            if len(history) == 1 and len(self.tokenizer.encode(history[-1][CONTENT], add_special_tokens=False)) > remaining:
                history[-1][CONTENT] = self.tokenizer.decode(self.tokenizer.encode(history[-1][CONTENT], add_special_tokens=False)[:-remaining])
            elif len(self.tokenizer.encode(output, add_special_tokens=False)) > remaining:
                cut_output = True
                output_ids = output_ids[:-remaining]
                output = self.tokenizer.decode(output_ids)
            elif len(self.tokenizer.encode(system[CONTENT], add_special_tokens=False)) > remaining:
                system_ids = self.tokenizer.encode(system[CONTENT], add_special_tokens=False)
                system_ids = system_ids[:-remaining]
                system[CONTENT] = self.tokenizer.decode(system_ids)
            else:
                sample_ids = sample_ids[remaining:]
                full_sample = self.tokenizer.decode(sample_ids)
                break
        if cut_output:
            full_sample = full_sample[:-1]
        full_sample = re.sub(r"\<\|im_start\|\>\s+user", r"<|im_start|>user", full_sample)
        full_sample = re.sub(r"\<\|im_assistant\|\>\s+assistant", r"<|im_assistant|>assistant", full_sample)
        self.logger.info(f"Full sample: {full_sample}")
        inputs = self.tokenizer(full_sample, add_special_tokens=False, return_tensors="pt")
        return {
            INPUT_IDS: inputs[INPUT_IDS][0],
            ATTENTION_MASK: inputs[ATTENTION_MASK][0]
        }
